export async function moduleFunction() {
    const response = {
        id: '5001902391',
        number: '5015',
        realAccountNumberLast4: '5015',
        accountNumberDisplay: '5015',
        name: 'Super Checking',
        balance: 1000.0,
        type: 'checking',
        status: 'active',
        customerId: '6000000928',
        institutionId: '101732',
        balanceDate: 1644279599,
        createdDate: 1644279599,
        currency: 'USD',
        institutionLoginId: 5000449190,
        displayPosition: 1,
        accountNickname: 'Super Checking',
        marketSegment: 'personal',
    }
    return response;
}